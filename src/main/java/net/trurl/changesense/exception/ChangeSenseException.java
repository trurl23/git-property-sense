package net.trurl.changesense.exception;

public class ChangeSenseException extends RuntimeException {

    ChangeSenseException(final String s) {
        super(s);
    }

    public ChangeSenseException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

    ChangeSenseException(final Throwable throwable) {
        super(throwable);
    }
}
