package net.trurl.changesense.ssh.impl;

import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;

/**
 * Transport callback that is used internally to install a suitable {@link SshSessionFactory} to handle authentication
 * using a specific private key. Does nothing if transport is not of type {@link SshTransport}.
 */
public class SshTransportConfigCallback implements TransportConfigCallback {
    /**
     * Session factory for SSH connections.
     */
    private final SshSessionFactory sessionFactory;

    public SshTransportConfigCallback(final CredentialsProvider credentialsProvider) {
        this.sessionFactory = new SshSessionFactory(credentialsProvider);
    }

    @Override
    public void configure(final Transport transport) {
        if (transport instanceof SshTransport) {
            final SshTransport sshTransport = (SshTransport) transport;
            sshTransport.setSshSessionFactory(sessionFactory);
        }
    }
}
