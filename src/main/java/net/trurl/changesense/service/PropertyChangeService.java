package net.trurl.changesense.service;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.PropertiesFileDifference;

import java.util.List;

/**
 * Get differences in properties (along with documentation) in certain paths between two revisions of a repository.
 */
public interface PropertyChangeService {

    /**
     * Calculate a list of property differences (including documentation) between the two specified revisions.
     *
     * @param repository   Change repository.
     * @param revisionA    Revision or tag name.
     * @param revisionB    Revision or tag name.
     * @param propertyPath List of paths in the repo denoting directories to search for property files.
     * @return a list of difference-objects, one for each property file that has changed.
     */
    List<PropertiesFileDifference> getPropertyDifferences(final ChangeRepository repository,
                                                          final String revisionA, final String revisionB,
                                                          final String... propertyPath);


}
