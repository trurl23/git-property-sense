package net.trurl.changesense.service;

import net.trurl.changesense.exception.ChangeSenseException;
import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.Configurable;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.model.impl.DefaultChangeSenseConfiguration;

import java.lang.reflect.InvocationTargetException;

/**
 * Service to create {@link ChangeRepository} instances.
 * <p>
 * Use {@link #createDefaultRepositoryService()} to create an instance of the default repository implementation using
 * the {@link DefaultChangeSenseConfiguration}.
 */
public interface RepositoryService extends Configurable {

    /**
     * Factory method to create default repository service using {@link DefaultChangeSenseConfiguration}.
     *
     * @return default repository instance.
     */
    static RepositoryService createDefaultRepositoryService() {
        return createDefaultRepositoryService(new DefaultChangeSenseConfiguration());
    }

    /**
     * Factory method to create default repository service implementation.
     *
     * @param changeSenseConfiguration Change sense configuration to use.
     * @return default repository service instance.
     */
    static RepositoryService createDefaultRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        if (changeSenseConfiguration == null) {
            throw new IllegalArgumentException("Configuration must not be null!");
        }
        if (changeSenseConfiguration.getRepositoryServiceClass() == null) {
            throw new IllegalStateException("Repository service class must not be null!");
        }
        try {
            return changeSenseConfiguration.getRepositoryServiceClass()
                    .getConstructor(ChangeSenseConfiguration.class)
                    .newInstance(changeSenseConfiguration);
        } catch (final InstantiationException | NoSuchMethodException | InvocationTargetException |
                IllegalAccessException e) {
            throw new ChangeSenseException(String.format("Error creating instance of '%s'!",
                    changeSenseConfiguration.getRepositoryServiceClass().getCanonicalName()), e);
        }
    }

    /**
     * Get repository service name.
     *
     * @return Service name, e.g. "git".
     */
    String getName();

    /**
     * Return true if this repository service is able to create a change repository from the given configuration
     * class.
     *
     * @param configurationClazz Configuration class.
     * @return true if a call to {@link #createChangeRepository(RepositoryConfiguration)} should produce a change
     * repository.
     */
    boolean accepts(final Class<? extends RepositoryConfiguration> configurationClazz);

    /**
     * Create change repository from the given configuration.
     *
     * @param repositoryConfiguration Repository configuration object.
     * @return change repository instance.
     */
    ChangeRepository createChangeRepository(final RepositoryConfiguration repositoryConfiguration);
}
