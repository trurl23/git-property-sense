package net.trurl.changesense.service.impl;

import net.trurl.changesense.exception.ChangeSenseGitException;
import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.model.impl.GitChangeRepository;
import net.trurl.changesense.model.impl.GitRemoteRepositoryConfiguration;
import net.trurl.changesense.ssh.impl.SshTransportConfigCallback;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Service that creates and updates git repositories.
 */
public class GitRemoteRepositoryService extends AbstractRepositoryService<GitRemoteRepositoryConfiguration> {

    /**
     * Create a new git repository service.
     *
     * @param changeSenseConfiguration Configuration object.
     */
    @SuppressWarnings("WeakerAccess")
    public GitRemoteRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        super(changeSenseConfiguration, GitRemoteRepositoryConfiguration.class);
    }
    
    /**
     * Returns true if the repo can be opened.
     *
     * @param config Remote repository configuration.
     * @return true if the repository at repoDir can be opened, false otherwise.
     */
    private boolean repositoryExists(final GitRemoteRepositoryConfiguration config) {
        try {
            Git.open(config.getRepoDir()).remoteList().call();
        } catch (final GitAPIException | IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * Create a repository instance using an already existing git repository. It is expected that the repository
     * already has a matching remote.
     *
     * @param repoConfig Repository configuration.
     * @return a {@link ChangeRepository} instance.
     */
    private ChangeRepository createRepositoryFromExistingGitRepository(final GitRemoteRepositoryConfiguration repoConfig) {
        final Map<String, String> remoteConfigs;
        try {
            final Git git = Git.open(repoConfig.getRepoDir());
            remoteConfigs = git.remoteList().call().stream()
                    .flatMap(config -> config.getURIs().stream()
                            .map(uri -> ImmutablePair.of(uri.toString(), config.getName())))
                    .collect(Collectors.toMap(pair -> pair.left, pair -> pair.right, (n1, n2) -> n1));
            if (!remoteConfigs.containsKey(repoConfig.getRemoteUrl())) {
                throw new IllegalStateException(String.format("Repo has no remote with URL '%s'!",
                        repoConfig.getRemoteUrl()));
            }
            git.fetch()
                    .setTransportConfigCallback(new SshTransportConfigCallback(repoConfig.getCredentialsProvider()))
                    .setCredentialsProvider(repoConfig.getCredentialsProvider())
                    .setRemote(remoteConfigs.get(repoConfig.getRemoteUrl())).call();
            return new GitChangeRepository(getConfig(), repoConfig.getRepoDir());
        } catch (final GitAPIException | IOException e) {
            throw new ChangeSenseGitException(String.format("Error determining repo remotes of repo '%s'!",
                    repoConfig.getRepoDir()), e);
        }
    }

    /**
     * Clone a repository from a remote.
     *
     * @param repoConfig Repository configuration.
     * @return a property repository.
     * @throws ChangeSenseGitException if the repo could not be initialized.
     */
    private ChangeRepository createRepositoryFromScratch(final GitRemoteRepositoryConfiguration repoConfig) {
        try {
            FileUtils.forceMkdir(repoConfig.getRepoDir());
        } catch (final IOException ex) {
            throw new ChangeSenseGitException(
                    String.format("Error creating git repo directory '%s'!", repoConfig.getRepoDir()), ex);
        }
        try {
            Git.cloneRepository()
                    .setDirectory(repoConfig.getRepoDir())
                    .setBare(true)
                    .setURI(repoConfig.getRemoteUrl())
                    .setTransportConfigCallback(new SshTransportConfigCallback(repoConfig.getCredentialsProvider()))
                    .setCredentialsProvider(repoConfig.getCredentialsProvider())
                    .call();
            return new GitChangeRepository(getConfig(), repoConfig.getRepoDir());
        } catch (final GitAPIException ex) {
            throw new ChangeSenseGitException(
                    String.format("Error initializing git repo at '%s' from '%s'!",
                            repoConfig.getRepoDir(), repoConfig.getRemoteUrl()), ex);
        }
    }

    @Override
    protected ChangeRepository doCreateChangeRepository(final GitRemoteRepositoryConfiguration repositoryConfiguration) {
        if (repositoryExists(repositoryConfiguration)) {
            return createRepositoryFromExistingGitRepository(repositoryConfiguration);
        }
        return createRepositoryFromScratch(repositoryConfiguration);
    }

    @Override
    protected Class<? extends RepositoryConfiguration> getConfigurationClass() {
        return GitRemoteRepositoryConfiguration.class;
    }

    @Override
    public String getName() {
        return "git-remote";
    }
}
