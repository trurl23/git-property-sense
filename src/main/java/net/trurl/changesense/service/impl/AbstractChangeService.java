package net.trurl.changesense.service.impl;

import net.trurl.changesense.exception.ChangeSenseGitException;
import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangedFile;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Base of all change services. Use {@link #calcChangedFiles(ChangeRepository, String, String, String, String...)} to
 * get a list of changed files from a repository.
 */
abstract class AbstractChangeService {
    /**
     * Get a list of changed files that were touched between the given revisions from a repository. Optionally use a
     * filename suffix (including the dot).
     *
     * @param repository     Repository to use.
     * @param revisionA      Reference to revision a, dependent on the {@link ChangeRepository} used.
     * @param revisionB      Reference to revision b, dependent on the {@link ChangeRepository} used.
     * @param filenameSuffix Filename suffix including the dot (optional).
     * @param filePaths      A number of repo file paths to filter which changes are picked up (optional).
     * @return a list of {@link ChangedFile} instances.
     */
    List<ChangedFile> calcChangedFiles(final ChangeRepository repository,
                                       final String revisionA, final String revisionB,
                                       final String filenameSuffix,
                                       final String... filePaths) {
        return repository.calcChangedFiles(revisionA, revisionB,
                createPathMatchingPattern(filenameSuffix, fixFilePaths(filePaths)));
    }

    /**
     * Sanitize repo file paths. Add root path when no file paths are specified.
     *
     * @param filePaths File paths to pick up changes from (optional).
     * @return sanitized file paths.
     */
    private String[] fixFilePaths(final String... filePaths) {
        if (filePaths.length == 0) {
            return new String[]{""};
        }
        return filePaths;
    }

    /**
     * Create a pattern containing the specified file paths along with the file suffix.
     *
     * @param fileSuffix File suffix with dot (optional).
     * @param filePath   File path (at least one must be provided).
     * @return a {@link Pattern} for matching file names.
     */
    private Pattern createPathMatchingPattern(final String fileSuffix, final String... filePath) {
        if (filePath.length == 0) {
            throw new ChangeSenseGitException("Must supply at least one file path!");
        }
        final String regex = "^" + StringUtils.join(Stream.of(filePath)
                .map(propPath -> String.format("(%s[^./]+%s)",
                        sanitizePathPattern(propPath),
                        sanitizeFileSuffix(fileSuffix)))
                .collect(Collectors.toList()), "|");
        return Pattern.compile(regex + "$");
    }

    /**
     * Sanitize path pattern. Applies {@link Pattern#quote(String)} to the file path and sanitizes "/".
     *
     * @param filePath File path.
     * @return sanitized file path.
     */
    private String sanitizePathPattern(final String filePath) {
        if ("/".equals(filePath)) {
            return "";
        }
        if (filePath.length() > 0 && !filePath.endsWith("/")) {
            return Pattern.quote(filePath + "/");
        }
        return Pattern.quote(filePath);
    }

    /**
     * Sanitizes file suffix.
     *
     * @param fileSuffix File suffix including dot (optional).
     * @return Sanitized and quoted file suffix.
     */
    private String sanitizeFileSuffix(final String fileSuffix) {
        if (StringUtils.isBlank(fileSuffix)) {
            return "/[^/]+";
        }
        return Pattern.quote(fileSuffix);
    }
}
