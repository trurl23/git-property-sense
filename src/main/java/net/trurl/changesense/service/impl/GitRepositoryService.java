package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.model.impl.GitChangeRepository;
import net.trurl.changesense.model.impl.GitRepositoryConfiguration;

/**
 * A repository service implementation that can create {@link GitChangeRepository} instances from
 * {@link GitRepositoryConfiguration} objects.
 */
public class GitRepositoryService extends AbstractRepositoryService<GitRepositoryConfiguration> {

    public GitRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        super(changeSenseConfiguration, GitRepositoryConfiguration.class);
    }

    @Override
    protected ChangeRepository doCreateChangeRepository(final GitRepositoryConfiguration repositoryConfiguration) {
        return new GitChangeRepository(getConfig(), repositoryConfiguration.getRepoDir());
    }

    @Override
    protected Class<? extends RepositoryConfiguration> getConfigurationClass() {
        return GitRepositoryConfiguration.class;
    }

    @Override
    public String getName() {
        return "git";
    }
}
