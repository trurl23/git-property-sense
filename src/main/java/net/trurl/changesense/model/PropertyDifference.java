package net.trurl.changesense.model;

import java.util.stream.Stream;

/**
 * Difference in the state of a property between two revisions.
 */
public interface PropertyDifference {

    enum DifferenceType {
        /**
         * Property has been added.
         */
        ADDED,
        /**
         * Property has been removed.
         */
        REMOVED,
        /**
         * Property has been changed.
         */
        CHANGED,
        /**
         * Property has not been touched.
         */
        UNCHANGED;

        /**
         * Returns true if the value is one of the given difference types.
         *
         * @param differenceTypes A list of difference types.
         * @return true if the value checked is one of the given types.
         */
        public boolean isAnyOf(final DifferenceType... differenceTypes) {
            return Stream.of(differenceTypes)
                    .anyMatch(diff -> this == diff);
        }

    }

    /**
     * Get type of difference of the property.
     *
     * @return difference type.
     */
    DifferenceType getDifferenceType();

    /**
     * Get state of property at revision A.
     *
     * @return state of property.
     */
    Property getBefore();

    /**
     * Get state of property at revision B.
     *
     * @return state of property.
     */
    Property getAfter();
}
