package net.trurl.changesense.model;

import java.util.List;
import java.util.regex.Pattern;

/**
 * A repository that can be used to determine which files have changed between two revisions as well as to retrieve
 * the contents of files that have changed.
 */
public interface ChangeRepository extends Configurable {
    /**
     * Find those files matching the filename pattern that have changed after revision A up to including revision B.
     *
     * @param revisionA       Repository reference to revision A.
     * @param revisionB       Repository reference to revision B.
     * @param filenamePattern Filename pattern to apply before picking up changes from the repo.
     * @return a list of changed files between revision A and revision B.
     */
    List<ChangedFile> calcChangedFiles(final String revisionA, final String revisionB, final Pattern filenamePattern);

    /**
     * Try to resolve the specified reference.
     *
     * @param revision Revision reference (depending on the underlying repository implementation).
     * @return true if the revision could be resolved successfully.
     */
    boolean tryResolveReference(final String revision);

    /**
     * Fetch old revision of changed file.
     *
     * @param changedFile Changed file instance.
     * @return contents of the file up to {@link ChangeSenseConfiguration#getMaximumSizeOfFilesBytes()}.
     */
    byte[] fetchOldRevision(ChangedFile changedFile);

    /**
     * Fetch new revision of changed file.
     *
     * @param changedFile Changed file instance.
     * @return contents of the file up to {@link ChangeSenseConfiguration#getMaximumSizeOfFilesBytes()}.
     */
    byte[] fetchNewRevision(ChangedFile changedFile);
}
