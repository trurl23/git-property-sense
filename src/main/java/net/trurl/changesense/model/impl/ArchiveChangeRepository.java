package net.trurl.changesense.model.impl;

import net.trurl.changesense.exception.ChangeSenseArchiveException;
import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.ChangedFile;
import net.trurl.changesense.model.RepositoryConfiguration;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A {@link ChangeRepository} to find changed files between two archives.
 * <p>
 * Use {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} with a
 * {@link ArchiveRepositoryConfiguration} object to obtain an instance of this repository.
 * <p>
 * Unlike versioning system repositories, paths to archive files are used as "revisions" by this implementation.
 * To compare two revisions, just pass the absolute paths of two archive files in the revision strings to
 * {@link #calcChangedFiles(String, String, Pattern)}. The repository will then calculate the differences between the
 * two directory trees contained in the archive, similar to a {@link FilesystemChangeRepository}.
 */
public class ArchiveChangeRepository implements ChangeRepository {

    /**
     * System configuration.
     */
    private final ChangeSenseConfiguration changeSenseConfiguration;

    /**
     * Create new archive change repository.
     *
     * @param changeSenseConfiguration System configuration.
     */
    public ArchiveChangeRepository(final ChangeSenseConfiguration changeSenseConfiguration) {
        this.changeSenseConfiguration = changeSenseConfiguration;
    }

    @Override
    public List<ChangedFile> calcChangedFiles(final String revisionA,
                                              final String revisionB,
                                              final Pattern filenamePattern) {
        if (StringUtils.isBlank(revisionA)) {
            throw new IllegalArgumentException("Archive name of original revision may not be blank!");
        }
        if (StringUtils.isBlank(revisionB)) {
            throw new IllegalArgumentException("Archive name of new revision may not be blank!");
        }
        try (final ArchiveAccess archiveA = ArchiveAccess.openArchive(new File(revisionA))) {
            try (final ArchiveAccess archiveB = ArchiveAccess.openArchive(new File(revisionB))) {
                return detectChangedFiles(archiveA, archiveB, filenamePattern);
            } catch (final IOException ex) {
                throw new ChangeSenseArchiveException(String.format("Error reading archive '%s'!", revisionB));
            }
        } catch (final IOException ex) {
            throw new ChangeSenseArchiveException(String.format("Error reading archive '%s'!", revisionA));
        }
    }

    /**
     * Detect list of changed files matching the specified file name pattern.
     *
     * @param revisionA       Original revision archive.
     * @param revisionB       New revision archive.
     * @param filenamePattern Files contained in the archives are matched against this file name pattern.
     * @return list of changed files between the two archives matching the pattern.
     */
    private List<ChangedFile> detectChangedFiles(final ArchiveAccess revisionA,
                                                 final ArchiveAccess revisionB,
                                                 final Pattern filenamePattern) {
        final Map<String, ArchiveEntry> entriesA = revisionA.fetchArchiveFileEntries(filenamePattern);
        final Map<String, ArchiveEntry> entriesB = revisionB.fetchArchiveFileEntries(filenamePattern);
        return Stream.concat(
                entriesB.values().stream()
                        .map(archiveEntry -> new ArchiveChangedFile(this, revisionA, revisionB,
                                entriesA.get(archiveEntry.getName()), archiveEntry)),
                entriesA.values().stream()
                        .filter(archiveEntry -> !entriesB.containsKey(archiveEntry.getName()))
                        .map(archiveEntry -> new ArchiveChangedFile(this, revisionA, revisionB,
                                archiveEntry, entriesB.get(archiveEntry.getName()))))
                .collect(Collectors.toList());


    }

    @Override
    public boolean tryResolveReference(final String revision) {
        return ArchiveAccess.openArchive(new File(revision)).check();
    }

    @Override
    public byte[] fetchOldRevision(final ChangedFile changedFile) {
        final ArchiveChangedFile archiveChangedFile = safeCast(changedFile);
        return archiveChangedFile.getRevisionA()
                .fetchFromArchive(changedFile.getFilename(), changeSenseConfiguration.getMaximumSizeOfFilesBytes());
    }

    @Override
    public byte[] fetchNewRevision(final ChangedFile changedFile) {
        final ArchiveChangedFile archiveChangedFile = safeCast(changedFile);
        return archiveChangedFile.getRevisionB()
                .fetchFromArchive(changedFile.getFilename(), changeSenseConfiguration.getMaximumSizeOfFilesBytes());
    }

    /**
     * Cast changed file safely to the matching implementation.
     *
     * @param changedFile Changed file instance, expected to be an {@link ArchiveChangedFile}.
     * @return an {@link ArchiveChangedFile} instance.
     */
    private ArchiveChangedFile safeCast(final ChangedFile changedFile) {
        if (!(changedFile instanceof ArchiveChangedFile)) {
            throw new IllegalArgumentException(String.format("Unexpected ChangedFile implementation: '%s'",
                    changedFile.getClass().getCanonicalName()));
        }
        return (ArchiveChangedFile) changedFile;
    }

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
