package net.trurl.changesense.model.impl;

import net.trurl.changesense.exception.ChangeSenseArchiveException;
import net.trurl.changesense.model.ChangeRepository;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.io.FileUtils;

import java.io.IOException;

/**
 * A {@link net.trurl.changesense.model.ChangedFile} implementation working with files contained in archive files.
 */
public class ArchiveChangedFile extends AbstractChangedFile {
    /**
     * Archive containing the original revision of files.
     */
    private final ArchiveAccess revisionA;

    /**
     * Archive containing the new revision of files.
     */
    private final ArchiveAccess revisionB;

    private final String filename;

    private final int changeType;

    private final ChangeRepository changeRepository;

    /**
     * Create archive changed-file instance. Either original or new revision of entry must not be null.
     *
     * @param changeRepository Parent repository.
     * @param revisionA        Archive containing original revision of files (for reading file data).
     * @param revisionB        Archive containing new revision of files (for reading file data).
     * @param entryA           Archive entry of original revision.
     * @param entryB           Archive entry of new revision.
     */
    ArchiveChangedFile(final ChangeRepository changeRepository,
                       final ArchiveAccess revisionA,
                       final ArchiveAccess revisionB,
                       final ArchiveEntry entryA,
                       final ArchiveEntry entryB) {
        super(changeRepository);
        this.revisionA = revisionA;
        this.revisionB = revisionB;
        if (entryA != null) {
            this.filename = entryA.getName();
        } else if (entryB != null) {
            this.filename = entryB.getName();
        } else {
            throw new IllegalArgumentException("Either entry A or entry B must not be null!");
        }
        this.changeRepository = changeRepository;
        this.changeType = determineChangeType(entryA, entryB);
    }

    /**
     * Determine type of change between entryA and entryB. Will resort to comparing file contents if file size is
     * unchanged.
     *
     * @param entryA Original revision's archive entry.
     * @param entryB New revision's archive entry.
     * @return ChangeType as int.
     */
    private int determineChangeType(
            final ArchiveEntry entryA,
            final ArchiveEntry entryB) {
        if (entryA == null) {
            return ChangeType.ADDED.getType();
        }
        if (entryB == null) {
            return ChangeType.REMOVED.getType();
        }
        if (entryA.getSize() != entryB.getSize()) {
            return ChangeType.CHANGED.getType();
        }
        if (!compareInputStreamsUpToMaxSize(entryA.getName(), revisionA, revisionB)) {
            return ChangeType.CHANGED.getType();
        }
        return 0;
    }

    /**
     * Compare input streams of file contents up to maximum size.
     *
     * @param filename Name of file.
     * @param archiveA Archive containing original revision.
     * @param archiveB Archive containing new revision.
     * @return true, if the files have the same contents.
     */
    private boolean compareInputStreamsUpToMaxSize(
            final String filename,
            final ArchiveAccess archiveA,
            final ArchiveAccess archiveB) {
        int valueA = 0;
        int valueB = 0;
        long read = 0;
        archiveA.skipToEntry(filename);
        archiveB.skipToEntry(filename);
        final int max = changeRepository.getConfig().getMaximumSizeOfFilesBytes();
        while (valueA == valueB && valueA != -1) {
            try {
                valueA = archiveA.read();
                valueB = archiveB.read();
            } catch (final IOException ex) {
                throw new ChangeSenseArchiveException(String.format("Error reading archives '%s', '%s'!",
                        revisionA, revisionB), ex);
            }
            read++;
            if (read >= max) {
                throw new ChangeSenseArchiveException(String.format("Size of file '%s' exceeds maximum file size of %s!",
                        max, FileUtils.byteCountToDisplaySize(max)));
            }
        }
        return valueA == valueB;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public int getChangeType() {
        return changeType;
    }

    /**
     * Get access to archive with original revisions.
     *
     * @return archive access.
     */
    ArchiveAccess getRevisionA() {
        return revisionA;
    }

    /**
     * Get access to archive with new revisions.
     *
     * @return archive access.
     */
    ArchiveAccess getRevisionB() {
        return revisionB;
    }
}
