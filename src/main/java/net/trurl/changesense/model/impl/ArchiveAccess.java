package net.trurl.changesense.model.impl;

import net.trurl.changesense.exception.ChangeSenseArchiveException;
import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Auto-closeable wrapper around {@link ArchiveInputStream} convenient archive access.
 * <p>
 * Use factory method {@link #openArchive(File)} to open an archive file.
 */
public class ArchiveAccess implements AutoCloseable {
    private final static Logger LOGGER = LoggerFactory.getLogger(ArchiveAccess.class);

    private final File archiveFile;

    private ArchiveInputStream openArchiveStream;

    /**
     * Open archive from file. Supports ar, cpio, Unix dump, tar, zip, gzip, XZ, Pack200, bzip2, 7z, arj, lzma, snappy,
     * DEFLATE, lz4, Brotli, Zstandard, DEFLATE64 and Z via
     * <a href="https://commons.apache.org/proper/commons-compress/index.html">Apache Commons Compress</a>.
     *
     * @param archiveFile Archive file.
     * @return archive access instance.
     */
    public static ArchiveAccess openArchive(final File archiveFile) {
        return new ArchiveAccess(archiveFile);
    }

    private ArchiveAccess(final File archiveFile) {
        this.archiveFile = archiveFile;
    }

    /**
     * Prepare a map of archive entries present in the archive that match the specified filenamePattern. Does not
     * change position in access instance.
     *
     * @param filenamePattern File names in the archive that match against this pattern will be returned in the map.
     * @return mapping of archive file names to archive entries, key set and values retain the order the files were
     * found in the archive listing.
     */
    public Map<String, ArchiveEntry> fetchArchiveFileEntries(final Pattern filenamePattern) {

        final List<ArchiveEntry> archiveEntries = fetchMatchingArchiveEntries(filenamePattern);

        return buildEntryMap(archiveEntries);
    }

    /**
     * Build order preserving map from the file names as keys to the archive entries as values.
     *
     * @param archiveEntries List of archive entries, order is preserved.
     * @return map with key set and values in the same order as the input list.
     */
    private Map<String, ArchiveEntry> buildEntryMap(final List<ArchiveEntry> archiveEntries) {
        final Map<String, ArchiveEntry> entryMap = new TreeMap<>();
        archiveEntries.stream()
                .sorted(Comparator.comparing(ArchiveEntry::getName))
                .forEach(archiveEntry -> {
                    if (!archiveEntry.isDirectory()) {
                        entryMap.put(archiveEntry.getName(), archiveEntry);
                    }
                });
        return entryMap;
    }

    /**
     * Fetch a list of archive entries with file names matching the specified pattern.
     *
     * @param filenamePattern Pattern that the returned files have to match.
     * @return list of matching files in the same order they were found.
     */
    private List<ArchiveEntry> fetchMatchingArchiveEntries(final Pattern filenamePattern) {
        final List<ArchiveEntry> archiveEntries = new LinkedList<>();
        ArchiveEntry current;
        try (final ArchiveInputStream archiveInputStream = createArchiveInputStreamWithWrappedException()) {
            while ((current = archiveInputStream.getNextEntry()) != null) {
                if (!archiveInputStream.canReadEntryData(current)) {
                    LOGGER.warn("Could not read entry data of '{}'!", current.getName());
                } else if (filenamePattern.matcher(current.getName()).matches()) {
                    archiveEntries.add(current);
                }
            }
        } catch (final IOException ex) {
            throw new ChangeSenseArchiveException(String.format("Error getting file list from archive '%s'!",
                    archiveFile), ex);
        }
        return archiveEntries;
    }

    /**
     * Create new archive input stream and wrap checked exceptions into {@link ChangeSenseArchiveException}s.
     *
     * @return new archive input stream.
     */
    private ArchiveInputStream createArchiveInputStreamWithWrappedException() {
        try {
            return createArchiveInputStream();
        } catch (final FileNotFoundException | ArchiveException e) {
            throw new ChangeSenseArchiveException(String.format("Error opening archive '%s'!", archiveFile));
        }
    }

    /**
     * Create archive input stream.
     *
     * @return archive input stream.
     * @throws FileNotFoundException if the archive file was not found.
     * @throws ArchiveException      if the archive file was not understood.
     */
    private ArchiveInputStream createArchiveInputStream()
            throws FileNotFoundException, ArchiveException {
        return new ArchiveStreamFactory().createArchiveInputStream(new BufferedInputStream(
                new FileInputStream(archiveFile)));
    }

    /**
     * Check if the archive file was found and there are entries recognized in the archive.
     *
     * @return true if the archive is valid and can be read.
     */
    public boolean check() {
        try (final ArchiveInputStream archiveInputStream = createArchiveInputStream()) {
            return archiveInputStream.getNextEntry() != null;
        } catch (final IOException | ArchiveException ex) {
            return false;
        }
    }

    /**
     * Fetch file data from archive.
     *
     * @param filename Name of entry to fetch.
     * @param maxSize  Maximum number of bytes to read.
     * @return a buffer containing file data.
     */
    public byte[] fetchFromArchive(final String filename, final int maxSize) {
        try (final ArchiveInputStream archiveInputStream = createArchiveInputStreamWithWrappedException()) {
            final ArchiveEntry archiveEntry = skipToEntry(filename, archiveInputStream);
            if (archiveEntry == null) {
                throw new ChangeSenseArchiveException(
                        String.format("Archive '%s' does not contain file named '%s'!", archiveFile, filename));
            }
            if (maxSize < 0) {
                throw new IllegalArgumentException("Can't read negative size!");
            }
            if (maxSize == 0) {
                return new byte[0];
            }
            final byte[] buffer = new byte[maxSize];
            IOUtils.readFully(archiveInputStream, buffer, 0, (int) Math.min(maxSize, archiveEntry.getSize()));
            return buffer;
        } catch (final IOException e) {
            throw new ChangeSenseArchiveException(String.format("Error closing archive '%s'!", archiveFile), e);
        }
    }

    /**
     * Skip access instance to the entry with the specified file name. Afterwards, byte data of the file can be
     * read.
     *
     * @param filename File name of entry.
     * @return the archive entry descriptor.
     */
    public ArchiveEntry skipToEntry(final String filename) {
        return skipToEntry(filename, ensureOpen());
    }

    /**
     * Skip input stream to the entry with the specified file name.
     *
     * @param filename    File name of entry.
     * @param inputStream Archive input stream.
     * @return the archive entry descriptor.
     */
    private ArchiveEntry skipToEntry(final String filename, final ArchiveInputStream inputStream) {
        final ArchiveInputStream validStream = inputStream != null ? inputStream : ensureOpen();
        ArchiveEntry current;
        do {
            try {
                current = validStream.getNextEntry();
            } catch (final IOException e) {
                throw new ChangeSenseArchiveException(String.format("Error reading next entry of archive '%s'!",
                        filename));
            }
        } while (current != null && !filename.equals(current.getName()));
        return current;
    }

    /**
     * Make sure the underlying archive input stream is open.
     *
     * @return an open archive input stream.
     */
    private ArchiveInputStream ensureOpen() {
        if (openArchiveStream == null) {
            openArchiveStream = createArchiveInputStreamWithWrappedException();
        }
        return openArchiveStream;
    }

    /**
     * Close the underlying archive input stream.
     *
     * @throws IOException if the stream could not be closed.
     */
    public void close() throws IOException {
        try {
            if (openArchiveStream != null) {
                openArchiveStream.close();
            }
        } finally {
            openArchiveStream = null;
        }
    }

    /**
     * Read byte of file data.
     *
     * @return file data or -1 for end of file.
     * @throws IOException if there was an error reading the archive data.
     */
    public int read() throws IOException {
        ensureOpen();
        return openArchiveStream.read();
    }

    /**
     * Get next entry in underlying archive input stream.
     *
     * @return next archive entry or null if end of archive is reached.
     * @throws IOException when an error reading the archive occurred.
     */
    public ArchiveEntry getNextEntry() throws IOException {
        ensureOpen();
        return openArchiveStream.getNextEntry();
    }
}
