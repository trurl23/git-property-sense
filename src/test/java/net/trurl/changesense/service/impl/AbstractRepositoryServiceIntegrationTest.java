package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.impl.ArchiveRepositoryConfiguration;
import net.trurl.changesense.model.impl.FilesystemRepositoryConfiguration;
import net.trurl.changesense.model.impl.GitRemoteRepositoryConfiguration;
import net.trurl.changesense.service.RepositoryService;
import net.trurl.changesense.testutil.TestConfiguration;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public abstract class AbstractRepositoryServiceIntegrationTest {
    final private RepositoryService repoService = RepositoryService.createDefaultRepositoryService();


    File createTempDir() throws IOException {
        final File tempDir = Files.createTempDirectory("integration").toFile();
        FileUtils.forceDeleteOnExit(tempDir);
        return tempDir;
    }

    ChangeRepository createRemoteGitRepo(final File dir) {
        return repoService.createChangeRepository(new GitRemoteRepositoryConfiguration(new File(dir, "repo"),
                TestConfiguration.getCredentialsProvider(TestConfiguration.getGitRepoUrl()), TestConfiguration.getGitRepoUrl()));
    }

    ChangeRepository createFilesystemRepository() {
        return repoService.createChangeRepository(new FilesystemRepositoryConfiguration());
    }

    ChangeRepository createArchiveRepository() {
        return repoService.createChangeRepository(new ArchiveRepositoryConfiguration());
    }

}
