package net.trurl.changesense.model;

import net.trurl.changesense.model.impl.DefaultChangeSenseConfiguration;
import net.trurl.changesense.model.impl.PropertiesImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class PropertiesImplTest {

    private static final String PROPS_VARIOUS_PROPERTIES = "/props/various.properties";
    private static final int PROPERTIES_COUNT = 10;

    private static final String PROP_WITH_VALUE = "prop.with.value";
    private static final String PROP_WITHOUT_VALUE = "prop.without.value";
    private static final String PROP_WITHOUT_DOCUMENTATION = "prop.without.documentation";
    private static final String PROP_MULTILINE = "multiline.property";
    private static final String PROP_FUNNY_NAME = "property.with.funny.name=";
    private static final String PROP_ESCAPE_TEST = "property.escape.test";
    private static final String PROP_WITH_SPACES = "prop.with.spaces";
    private static final String PROP_WITHOUT_SPACES = "prop.without.spaces";
    private static final String PROP_WITH_MANY_SPACES = "prop.with.many.spaces";
    private static final String PROP_WITHOUT_VALUE_AND_EQUALS = "prop.without.value.and.equals";

    private static final List<String> EXPECTED_PROPERTIES = Arrays.asList(
            PROP_WITH_VALUE,
            PROP_WITHOUT_VALUE,
            PROP_WITHOUT_DOCUMENTATION,
            PROP_MULTILINE,
            PROP_FUNNY_NAME,
            PROP_ESCAPE_TEST,
            PROP_WITH_SPACES,
            PROP_WITHOUT_SPACES,
            PROP_WITH_MANY_SPACES,
            PROP_WITHOUT_VALUE_AND_EQUALS);
    private static final List<String> EXPECTED_DOCUMENTATION = Arrays.asList(
            String.format("# Here's our first property documentation.%n#%n"),
            String.format("#%n# Here's the second property documentation.%n#%n"),
            "",
            String.format("# Multiline properties are supported.%n"),
            String.format("# Testing corner cases.%n"),
            String.format("# Testing escaping%n"),
            String.format("# should be \"oldie\"%n"),
            String.format("# should be \"but\"%n"),
            String.format("# should be \"goldie\"%n"),
            String.format("# Should have empty value.%n"));

    private static final List<String> EXPECTED_VALUES = Arrays.asList(
            "Value 39832",
            "",
            "Happy & Ignorant",
            "Some value, which continues on the next line.",
            "property value.",
            "\\\n",
            "oldie",
            "but",
            "goldie",
            "");

    /**
     * Make sure that various properties are read correctly.
     *
     * @throws IOException when something goes wrong.
     */
    @Test
    public void readFromShouldParseCorrectly() throws IOException {
        try (final InputStream inputStream = PropertiesImplTest.class.getResourceAsStream(PROPS_VARIOUS_PROPERTIES)) {
            final PropertiesImpl properties = new PropertiesImpl(new DefaultChangeSenseConfiguration());
            properties.readFrom(inputStream, "properties");
            assertEquals(PROPERTIES_COUNT, properties.getProperties().size());
            for (int i = 0; i < EXPECTED_PROPERTIES.size(); i++) {
                final String expected = EXPECTED_PROPERTIES.get(i);
                assertTrue(properties.hasProperty(expected));
                final Property property = properties.getProperty(expected);
                assertNotNull(property);
                assertEquals(EXPECTED_DOCUMENTATION.get(i), property.getDocumentation());
                assertEquals(expected, property.getName());
                assertEquals(EXPECTED_VALUES.get(i), property.getValue());
            }
        }
    }
}