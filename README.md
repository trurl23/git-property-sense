# What is change-sense?

``change-sense`` is a Java library to detect changes between git 
revisions primarily aimed at easy to use properties based configuration 
management with a minimal dependency footprint.

It is based on JGit for java git-support.

### Releases

Download the [latest releases](https://www.trurl.net/change-sense/download.html).

View [javadoc of the latest release](https://www.trurl.net/change-sense/docs/index.html).

### Usage

For checking remote repositories for changes between revisions the 
``ChangeService`` provides a high-level interface for obtaining a 
``ChangeRepository`` from a remote repository, which in turn is able to 
calculate changes between two given revisions of a repository.

The remote git repository is cloned as a bare repository if it does
not exist within the configured path, otherwise it is updated from
the remote.

### Detecting File Changes

Below is an example showing how to obtain a ``ChangeRepository`` for a remote 
repository. 
This uses the default configuration which uses a new temporary directory
each time you create a change repository. For reusing cloned 
repositories and not littering your hard disk you should provide a 
directory via a configuration object implementing the 
ChangeSenseConfiguration interface.

```java
RepositoryConfiguration config = new GitRemoteRepositoryConfiguration(
        new File("/path/to/repo/dir"),
        new UsernamePasswordCredentialsProvider("foo", "bar"),
        "git@gitlab.com:trurl23/git-property-sense.git"
);

ChangeRepository changeRepository = RepositoryService.createRepository(
        config);
```

You could then use the ``ChangeRepository`` for example to detect added 
and removed properties files files between two revisions as follows (you
can of course list changes of other file-types).

```java
List<ChangedFile> changes = changeRepository.calcChangedFiles(
        "git-rev-a", "git-rev-b", Pattern.compile("^.*\\.properties$"));

List<ChangedFile> addedAndRemovedProperties = changes.stream()
    .filter(changedFile -> changedFile.is(ChangeType.ADDED, 
            ChangeType.REMOVED))
    .collect(Collectors.toList());

// Get data
if (changedFile.is(ChangeType.ADDED)) {
    // Get data of file at revision git-rev-b
    byte [] fileData = changedFile.fetchNewRevision();
    // Do something with it
}
```
### Detecting Changes in File Contents

There is special support for detecting changes of contents of files that 
store good old java properties (which can easily be enhanced to 
understand other key-value formats) with a twist: change-sense will
pick up configuration keys and values along with their documentation, 
so you can display changed property values along with it.

Properties files are expected to conform to the format laid out in the 
following example:

```properties
#
# This is some generic file header documentation which is ignored.
#######################################################################

# This is obviously some documentation for this property value
# which continues to line two.
property.value.a=foo

#
# Some header in-between, which is also ignored.
#

# This is the documentation for the property value below.
property.value.b=bar

#
# Some last words, which are - sadly - also ignored. 
#
```

The example below shows how to get all properties that changed in any
file with the file-ending ``.properties`` in two directories within the 
repo between two git tags ``git-tag-a`` and ``git-tag-b`` (you can
use any type of git reference, e.g. git revisions or branch names):

```java
PropertyChangeService propertyChangeService = 
    new PropertyChangeServiceImpl();

List<PropertiesFileDifference> differences = propertyChangeService
    .getPropertyDifferences(
        changeRepository, "git-tag-a^", "git-tag-b", 
        "path/1/in/repository", "path/2/in/repository");
```

Each ``PropertiesFileDifference`` instance will give you access to the
properties of a file along with their change status. 

The following example illustrates the process:

```java
if (!differences.isEmpty()) {
    PropertiesFileDifference fileDiff = differences.get(0);
    
    // Changed file gives you low-level access to the related changes
    ChangedFile changedFile = fileDiff.getFile();
    // Will print file name in the repo
    System.out.println(changedFile.getFilename());
    
    // Get all properties that have been added or removed
    List<PropertyDifference> propDiffs = fileDiff.getProperties(
            PropertyDifference.DifferenceType.ADDED, 
            PropertyDifference.DifferenceType.REMOVED);
}
```

In turn you can then examine each ``PropertyDifference`` instance:

```java
if (!propDiffs.isEmpty()) {
    
    // Get property difference to examine
    PropertyDifference propDiff = propDiffs.get(0);
    Property propState;
    
    if (propDiff.getDifferenceType() == PropertyDifference.
            DifferenceType.ADDED) {
        // Property was added, get state at the second revision
        propState = propDiff.getAfter();
    } else {
        // Property was removed, get state at the first revision
        propState = propDiff.getBefore();
    }
    
    // Will print:
    // # Documentation ...
    // name=value
    System.out.println(String.format("%s%n%s=%s", 
        propState.getDocumentation(),
        propState.getName(), propState.getValue()));
    
}
```